from Obstacle import Obstacle
import Const
import arcade


def is_on_screen(x, y, fix=7) -> (int, int):
    """Check if object is out of screen"""
    if x > Const.SCREEN_WIDTH - fix: x = Const.SCREEN_WIDTH - fix
    elif x < 0: x = fix
    if y > Const.SCREEN_HEIGHT - fix: y = Const.SCREEN_HEIGHT - fix
    elif y < 0: y = fix
    return x, y


class UserMap:
    def __init__(self):
        self.start_x = None
        self.start_y = None
        self.goal_x = None
        self.goal_y = None
        self.obstacles = []
        self.temp_obstacles = []
        self.drawing = False
        self.temp_x = 0
        self.temp_y = 0
        self.init_x = 0
        self.init_y = 0
        self.left = 0
        self.right = 0
        self.top = 0
        self.bottom = 0
        self.move = 0
        self.set_points()

    def set_points(self):
        self.start_x = Const.START_X
        self.start_y = Const.START_Y
        self.goal_x = Const.GOAL_X
        self.goal_y = Const.GOAL_Y

    def set_initial_position(self, x, y):
        if x > Const.SCREEN_WIDTH:  # cannot draw on the side bar
            self.drawing = False
            return
        self.init_x = self.temp_x = x
        self.init_y = self.temp_y = y

    def set_obstacle(self):
        if self.can_i_draw():
            self.temp_obstacles.append(Obstacle(self.left, self.right, self.top, self.bottom))
        self.left = self.right = self.top = self.bottom = 0

    def can_i_draw(self):
        """Obstacle cannot be too thin"""
        if self.right - self.left < 8 or self.top - self.bottom < 8:
            return False
        """Obstacle cannon be on the top of the goal or start point"""
        if self.check_collision(self.start_x, self.start_y) or self.check_collision(self.goal_x, self.goal_y):
            return False
        return True

    def undo(self):
        if len(self.temp_obstacles) > 0:
            self.temp_obstacles.pop()

    def clear(self):
        self.temp_obstacles = []

    def cancel(self):
        self.temp_obstacles = self.obstacles.copy()

    def check_collision(self, x, y, fix=20) -> bool:
        if self.left < x + fix and self.right > x - fix and self.bottom < y + fix and self.top > y - fix:
            return True
        else:
            return False

    def set_drawing(self, x, y):
        self.temp_x, self.temp_y = is_on_screen(x, y, 0)
        if self.init_x < self.temp_x:
            self.left = self.init_x
            self.right = self.temp_x
        else:
            self.left = self.temp_x
            self.right = self.init_x
        if self.init_y > self.temp_y:
            self.top = self.init_y
            self.bottom = self.temp_y
        else:
            self.top = self.temp_y
            self.bottom = self.init_y

    def show_drawing(self):
        """Draw helping lines"""
        if self.can_i_draw():
            arcade.draw_lrtb_rectangle_outline(self.left, self.right, self.top, self.bottom, arcade.color.GREEN, 2)
        else:
            arcade.draw_lrtb_rectangle_outline(self.left, self.right, self.top, self.bottom, arcade.color.RED, 2)

    def collide_with_obstacle(self, x, y, fix=5):
        """Check if the point is not on obstackles"""
        for a in self.temp_obstacles:
            if a.right + fix> x > a.left - fix and a.top + fix > y > a.bottom - fix:
                return True
        return False

    def move_start_point(self, x, y):
        if self.collide_with_obstacle(x, y): return
        x, y = is_on_screen(x, y)
        self.start_x = x
        self.start_y = y

    def move_finish_point(self, x, y):
        if self.collide_with_obstacle(x, y): return
        x, y = is_on_screen(x, y)
        self.goal_x = x
        self.goal_y = y

    def confirm(self):
        self.obstacles = self.temp_obstacles.copy()
        Const.START_X = self.start_x
        Const.START_Y = self.start_y
        Const.GOAL_X = self.goal_x
        Const.GOAL_Y = self.goal_y

    def show(self):
        arcade.draw_text(Const.TEXTS[67], 675, 520, arcade.color.WHITE, 24, align="center", anchor_x="center")
        arcade.draw_text(Const.TEXTS[68], 623, 450, arcade.color.WHITE, 14, align="center")
        arcade.draw_text(Const.TEXTS[69], 623, 420, arcade.color.WHITE, 14, align="center")
        arcade.draw_text(Const.TEXTS[70], 623, 390, arcade.color.WHITE, 14, align="center")
        arcade.draw_text(Const.TEXTS[71], 623, 360, arcade.color.WHITE, 14, align="center")
        arcade.draw_circle_outline(629, 319, 6, arcade.color.LIGHT_GRAY, 2)
        arcade.draw_text(Const.TEXTS[0], 650, 310, arcade.color.WHITE, 14, align="center")
        arcade.draw_circle_filled(629, 289, 7, arcade.color.RED)
        arcade.draw_text(Const.TEXTS[73], 650, 280, arcade.color.WHITE, 14, align="center")
        arcade.draw_circle_outline(self.start_x, self.start_y, 7, arcade.color.LIGHT_GRAY, 2)  # draw start point
        arcade.draw_circle_filled(self.goal_x, self.goal_y, 8, arcade.color.RED)   # draw goal
        for a in self.temp_obstacles:
            a.show()

    def get_obstacles(self):
        return self.obstacles.copy()

