import random


class Brain:
    def __init__(self, steps):
        self.step = 0
        self.maxSteps = steps
        self.direction = None

    def update(self, feature, p):
        self.direction = [None] * self.maxSteps
        self.direction[0] = random.random() * 360 - 180
        for i in range(self.maxSteps - 1):
            if random.random() < p:
                self.direction[i + 1] = random.random() * (feature * 2) - feature
            else:
                self.direction[i + 1] = random.random() * 30 - 15

    def clone_code(self, max_steps):
        clone = Brain(max_steps)
        clone.direction = self.direction[0:max_steps]
        return clone

    def mutate(self, rate):
        mutate_rate = rate  # 1% "genomu" zostanie zmodyfikowane
        for i in range(self.maxSteps):
            rand = random.random() * 100  # losowanie liczby od 0 do 100
            if rand < mutate_rate:  # jezeli rand jest mniejszy niz dana skala mutacji to zmien w i-tym elemencie kierunek poruszania sie
                change_angle = random.random() * 30 - 15
                self.direction[i] = change_angle

    def __del__(self):
        del self.direction
        del self.step
        del self.maxSteps
