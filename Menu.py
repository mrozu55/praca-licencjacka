from Button import TextButton
import Const
import arcade
import WN
import gc


def read_language(file) -> bool:
    import pickle
    try:
        with open(file, "rb") as binary_file:
            Const.TEXTS = pickle.load(binary_file)
            return True
    except IOError:
        WN.Window().prompt(Const.TEXTS[29], Const.TEXTS[53])
        return False


class MainMenu:
    def __init__(self, main):
        self.button_list = []
        self.lock = False
        self.main = main

    def setup(self):
        pos = 105
        stop_button = TextButton(Const.TEXTS[0], 679, 205, 18, self.pause_program)
        map_one_button = TextButton(Const.TEXTS[1], Const.SCREEN_WIDTH / 2 - 105, 415 + pos, 18, self.change_map_one)
        map_two_button = TextButton(Const.TEXTS[2], Const.SCREEN_WIDTH / 2, 415 + pos, 18, self.change_map_two)
        map_three_button = TextButton(Const.TEXTS[3], Const.SCREEN_WIDTH / 2 + 105, 415 + pos, 18,
                                      self.change_map_three)
        tournament_button = TextButton(Const.TEXTS[4], Const.SCREEN_WIDTH / 2 - 157, 325 + pos, 15,
                                       self.tournament_method)
        roulette_button = TextButton(Const.TEXTS[5], Const.SCREEN_WIDTH / 2 - 52, 325 + pos, 15, self.roulette_method)
        rank_button = TextButton(Const.TEXTS[6], Const.SCREEN_WIDTH / 2 + 53, 325 + pos, 15, self.rank_method)
        trunk_button = TextButton(Const.TEXTS[7], Const.SCREEN_WIDTH / 2 + 158, 325 + pos, 15, self.trunk_method)
        crossover_button = TextButton(Const.TEXTS[8] + (Const.TEXTS[27] if Const.CROSSOVER else Const.TEXTS[28]), Const.SCREEN_WIDTH / 2 - 52, 235 + pos, 15,
                                      self.crossover_method)
        parents_nr_button = TextButton(Const.TEXTS[9], Const.SCREEN_WIDTH / 2 + 53, 235 + pos, 15, self.parents_method)
        mutation_button = TextButton(Const.TEXTS[10] + (Const.TEXTS[27] if Const.MUTATION else Const.TEXTS[28]), Const.SCREEN_WIDTH / 2 - 52, 190 + pos, 15,
                                     self.mutation_method)
        change_mut_button = TextButton(Const.TEXTS[11], Const.SCREEN_WIDTH / 2 + 53, 190 + pos, 15,
                                       self.chenge_mut_method)
        incremental_button = TextButton(Const.TEXTS[12], Const.SCREEN_WIDTH / 2, 100 + pos, 15, self.incremental_method)
        population_button = TextButton(Const.TEXTS[13], Const.SCREEN_WIDTH / 2, 55 + pos, 15, self.population_method)
        speed_button = TextButton(Const.TEXTS[14] + str(self.main.speed), Const.SCREEN_WIDTH / 2, 10 + pos, 15, self.speed_method)
        info_button = TextButton(Const.TEXTS[15], 65, 35, 18, self.info_method)
        plot_button = TextButton(Const.TEXTS[16], 65, 80, 15, self.plot2_method)
        plot2_button = TextButton(Const.TEXTS[17], 65, 125, 15, self.plot_method)
        language_button = TextButton('ENG', Const.SCREEN_WIDTH - 35, 572, 15, self.lang_method, 42, 26)

        map_one_button.button_on(True)
        tournament_button.button_on(True)

        self.button_list.append(stop_button)  # 0
        self.button_list.append(map_one_button)  # 1
        self.button_list.append(map_two_button)  # 2
        self.button_list.append(map_three_button)  # 3
        self.button_list.append(tournament_button)  # 4
        self.button_list.append(roulette_button)  # 5
        self.button_list.append(rank_button)  # 6
        self.button_list.append(trunk_button)  # 7
        self.button_list.append(crossover_button)  # 8
        self.button_list.append(parents_nr_button)  # 9
        self.button_list.append(mutation_button)  # 10
        self.button_list.append(change_mut_button)  # 11
        self.button_list.append(incremental_button)  # 12
        self.button_list.append(population_button)  # 13
        self.button_list.append(speed_button)  # 14
        self.button_list.append(info_button)  # 15
        self.button_list.append(plot_button)  # 16
        self.button_list.append(plot2_button)  # 17
        self.button_list.append(language_button)  # 18
        self.change_speed_color()

    def draw_start(self):
        self.button_list[0].draw()

    def draw(self):
        arcade.draw_text(Const.TEXTS[23], Const.SCREEN_WIDTH / 2, 550, arcade.color.WHITE, 15, anchor_x="center")
        arcade.draw_text(Const.TEXTS[24], Const.SCREEN_WIDTH / 2, 460, arcade.color.WHITE, 15, anchor_x="center")
        arcade.draw_text(Const.TEXTS[25], Const.SCREEN_WIDTH / 2, 370, arcade.color.WHITE, 15, anchor_x="center")
        arcade.draw_text(Const.TEXTS[26], Const.SCREEN_WIDTH / 2, 235, arcade.color.WHITE, 15, anchor_x="center")
        arcade.draw_text(Const.TEXTS[72], Const.SCREEN_WIDTH - 90, 12, arcade.color.LIGHT_GRAY, 12, anchor_x="center")
        for i in range(len(self.button_list) - 1):
            self.button_list[i + 1].draw()

    def pause_program(self):
        if self.main.pause is True:
            self.button_list[0].change_title("Stop")
            self.main.pause = False
        else:
            self.button_list[0].change_title("Start")
            self.main.pause = True

    def change_map_one(self):
        self.button_list[1].button_on(True)
        self.button_list[2].button_on(False)
        self.button_list[3].button_on(False)
        self.main.map.change_map(1)
        self.restart()  # przed zmianna mapy nalezy zrestartowac wszystkie osobniki oraz liczbe generacji i krokow

    def change_map_two(self):
        Const.MAPS = 2
        self.button_list[1].button_on(False)
        self.button_list[2].button_on(True)
        self.button_list[3].button_on(False)
        self.main.map.change_map(2)
        self.restart()

    def change_map_three(self):
        self.button_list[1].button_on(False)
        self.button_list[2].button_on(False)
        self.button_list[3].button_on(True)
        self.main.map.change_map(3)
        self.restart()

    def restart(self):
        self.main.pop.restart_population()
        self.main.steps = "-"
        self.main.only_the_best = False
        self.main.generation = 1
        self.main.graph.restart()
        self.main.setup_goal()
        self.main.tick = 0
        if self.main.pop.incremental:
            self.main.pop.min_step = 30
            self.main.steps = self.main.pop.min_step
        gc.collect()  # garbage collector

    def tournament_method(self):
        if Const.PARENT_SELECTION == 1:
            self.lock = True
            wynik = ''
            while wynik is not None:
                wynik = WN.Window().input(Const.TEXTS[31],
                                          Const.TEXTS[33].format(
                                              Const.TOURNAMENT_PARAM))
                if wynik is None: break
                try:
                    param = int(wynik)
                    if param < 2:
                        WN.Window().prompt(Const.TEXTS[29], Const.TEXTS[34] + '2')
                        continue
                    if param > 50:
                        WN.Window().prompt(Const.TEXTS[30],
                                           Const.TEXTS[35] + ' (50)')
                        Const.TOURNAMENT_PARAM = 50
                        break
                    Const.TOURNAMENT_PARAM = param
                    break
                except ValueError:
                    WN.Window().prompt(Const.TEXTS[29], Const.TEXTS[36])
            self.lock = False
        else:
            Const.PARENT_SELECTION = 1
            self.button_list[4].button_on(True)
            self.button_list[5].button_on(False)
            self.button_list[6].button_on(False)
            self.button_list[7].button_on(False)

    def roulette_method(self):
        Const.PARENT_SELECTION = 2
        self.button_list[4].button_on(False)
        self.button_list[5].button_on(True)
        self.button_list[6].button_on(False)
        self.button_list[7].button_on(False)

    def rank_method(self):
        Const.PARENT_SELECTION = 3
        self.button_list[4].button_on(False)
        self.button_list[5].button_on(False)
        self.button_list[6].button_on(True)
        self.button_list[7].button_on(False)

    def trunk_method(self):
        if Const.PARENT_SELECTION == 4:
            self.lock = True
            wynik = ''
            trunk_param = int(Const.TRUNK_PARAM * 100)
            while wynik is not None:
                wynik = WN.Window().input(Const.TEXTS[31], Const.TEXTS[32].format(trunk_param))
                if wynik is None: break
                try:
                    param = int(wynik)
                    if param < 10:
                        WN.Window().prompt(Const.TEXTS[29], Const.TEXTS[34] + '10%')
                        continue
                    if param > 50:
                        WN.Window().prompt(Const.TEXTS[30], Const.TEXTS[35] + '(50%)')
                        Const.TRUNK_PARAM = 0.5
                        break
                    Const.TRUNK_PARAM = round(float(param / 100), 2)
                    break
                except ValueError:
                    WN.Window().prompt(Const.TEXTS[29], Const.TEXTS[36])
            self.lock = False
        else:
            Const.PARENT_SELECTION = 4
            self.button_list[4].button_on(False)
            self.button_list[5].button_on(False)
            self.button_list[6].button_on(False)
            self.button_list[7].button_on(True)

    def crossover_method(self):
        Const.CROSSOVER = False if Const.CROSSOVER else True
        self.button_list[8].change_title(Const.TEXTS[8] + (Const.TEXTS[27] if Const.CROSSOVER else Const.TEXTS[28]))
        self.change_cross_color()

    def parents_method(self):
        self.lock = True
        wynik = ''
        maks = Const.POPULATION if Const.POPULATION <= 800 else 800
        while wynik is not None:
            wynik = WN.Window().input(Const.TEXTS[31], Const.TEXTS[37].format(maks))
            if wynik is None: break
            try:
                parents = int(wynik)
                if parents < 2:
                    WN.Window().prompt(Const.TEXTS[29], Const.TEXTS[34] + '2')
                    continue
                if parents > Const.POPULATION or parents > 800:
                    WN.Window().prompt(Const.TEXTS[30], Const.TEXTS[35])
                    Const.PARENTS_NR = Const.POPULATION if Const.POPULATION <= 800 else 800
                    break
                Const.PARENTS_NR = parents
                break
            except ValueError:
                WN.Window().prompt(Const.TEXTS[29], Const.TEXTS[36])
        self.change_cross_color()  # Sprawdz czy nie trzeba zmienić koloru czcionki
        self.lock = False

    def change_mut_color(self):
        """Zmiana koloru czcionki mutacji w prawym panelu"""
        if Const.MUTATION:
            if Const.MUTATION_RATE <= 5:
                self.main.mut_color = arcade.color.WHITE
            elif 5 < Const.MUTATION_RATE <= 15:
                self.main.mut_color = arcade.color.ORANGE
            else:
                self.main.mut_color = arcade.color.RED
        else:
            self.main.mut_color = arcade.color.WHITE

    def change_cross_color(self):
        """Zmiana koloru czcionki l. rodzicow w prawym panelu"""
        if Const.CROSSOVER:
            if Const.PARENTS_NR < 300:
                self.main.cross_color = arcade.color.WHITE
            elif 300 <= Const.PARENTS_NR <= 600:
                self.main.cross_color = arcade.color.ORANGE
            else:
                self.main.cross_color = arcade.color.RED
        else:
            self.main.cross_color = arcade.color.WHITE

    def mutation_method(self):
        Const.MUTATION = False if Const.MUTATION else True
        self.button_list[10].change_title(Const.TEXTS[10] + (Const.TEXTS[27] if Const.MUTATION else Const.TEXTS[28]))
        self.change_mut_color()

    def incremental_method(self):
        self.restart()
        if self.main.pop.incremental is False:
            self.button_list[12].button_on(True)
            self.main.pop.incremental_method()
            self.main.steps = self.main.pop.min_step
        else:
            self.button_list[12].button_on(False)
            self.main.pop.incremental_method()
            self.main.steps = '-'

    def chenge_mut_method(self):
        self.lock = True
        var2 = ''
        while var2 is not None:
            var2 = WN.Window().input(Const.TEXTS[31], Const.TEXTS[38])
            if var2 is None: break
            try:
                mutation = float(var2)
                if mutation < 0:
                    WN.Window().prompt(Const.TEXTS[29], Const.TEXTS[39])
                    continue
                if mutation < 0.0001:
                    WN.Window().prompt(Const.TEXTS[30], Const.TEXTS[40])
                    Const.MUTATION_RATE = 0.0001
                    break
                if mutation > 100: mutation = 100.0
                if mutation > 5:  WN.Window().prompt(Const.TEXTS[30], Const.TEXTS[41])
                Const.MUTATION_RATE = mutation
                break
            except ValueError:
                WN.Window().prompt(Const.TEXTS[29], Const.TEXTS[36])

        self.change_mut_color()
        self.lock = False

    def population_method(self):
        self.lock = True
        wynik = ''
        while wynik is not None:
            wynik = WN.Window().input(Const.TEXTS[31],
                                      Const.TEXTS[42].format(
                                          Const.POPULATION))
            if wynik is None: break
            try:
                populacja = int(wynik)
                if populacja < 100:
                    WN.Window().prompt(Const.TEXTS[29], Const.TEXTS[34] + ' 100')
                    continue
                elif 1000 < populacja <= 5000:
                    var = WN.Window().confirm(Const.TEXTS[43])
                    if var == 'Yes':
                        Const.POPULATION = populacja
                        if self.main.generation == 1: self.restart()
                    break
                elif populacja > 5000:
                    WN.Window().prompt(Const.TEXTS[30], Const.TEXTS[35])
                    Const.POPULATION = 5000
                    if self.main.generation == 1: self.restart()
                    break
                Const.POPULATION = populacja
                if self.main.generation == 1: self.restart()
                if Const.PARENTS_NR > populacja:
                    Const.PARENTS_NR = populacja
                break
            except ValueError:
                WN.Window().prompt(Const.TEXTS[29], Const.TEXTS[36])

        """Zmiana koloru wyświetlanego tekstu"""
        if Const.POPULATION <= 1000:
            self.main.pop_color = arcade.color.WHITE
        elif 1000 < Const.POPULATION <= 2500:
            self.main.pop_color = arcade.color.ORANGE
        else:
            self.main.pop_color = arcade.color.RED

        self.lock = False

    def info_method(self):
        self.lock = True
        file = None
        try:
            file = open('Data/lang/'+Const.TEXTS[44], "r", encoding="utf-8")
            WN.Window().instruction(file)
        except IOError:
            WN.Window().prompt(Const.TEXTS[29], Const.TEXTS[45])
            self.lock = False
        finally:
            if not file.closed:
                file.close()
        self.lock = False

    def change_speed_color(self):
        if self.main.speed == 1:
            self.button_list[14].change_color(arcade.color.LIGHT_GRAY)
        elif self.main.speed == 2:
            self.button_list[14].change_color(arcade.color.ARYLIDE_YELLOW)
        else:
            self.button_list[14].change_color(arcade.color.LIGHT_RED_OCHRE)

    def speed_method(self):
        """Metoda zwiększająca predkość"""
        if self.main.speed == 1:
            arcade.schedule(self.main.update, 1 / 30)
            self.button_list[14].change_title(Const.TEXTS[14] + '2')
            self.main.speed = 2
        elif self.main.speed == 2:
            arcade.schedule(self.main.update, 1 / 60)
            self.button_list[14].change_title(Const.TEXTS[14] + '3')
            self.main.speed = 3
        else:
            arcade.unschedule(self.main.update)
            arcade.schedule(self.main.update, 1 / 60)
            self.button_list[14].change_title(Const.TEXTS[14] + '1')
            self.main.speed = 1
        self.change_speed_color()

    def plot_method(self):
        self.lock = True
        if self.main.generation <= 2:
            WN.Window().prompt(Const.TEXTS[29], Const.TEXTS[46])
        else:
            self.main.graph.plot_method()
        self.lock = False

    def plot2_method(self):
        self.lock = True
        if self.main.generation <= 2:
            WN.Window().prompt(Const.TEXTS[29], Const.TEXTS[46])
        else:
            self.main.graph.plot2_method()
        self.lock = False

    def lang_method(self):
        if Const.LANG == 0:
            if read_language('Data/lang/eng.dat') is False: return
            self.button_list[18].change_title('PL')
            Const.LANG = 1
        else:
            if read_language('Data/lang/pl.dat') is False: return
            self.button_list[18].change_title('ENG')
            Const.LANG = 0
        self.set_buttons()
        gc.collect()
        
    def set_buttons(self):
        for i in range(len(self.button_list) - 1):
            if i == 8: self.button_list[i].change_title(Const.TEXTS[8] + (Const.TEXTS[27] if Const.CROSSOVER else Const.TEXTS[28]))
            elif i == 10: self.button_list[i].change_title(Const.TEXTS[10] + (Const.TEXTS[27] if Const.MUTATION else Const.TEXTS[28]))
            elif i == 14: self.button_list[i].change_title(Const.TEXTS[14] + str(self.main.speed))
            else: self.button_list[i].change_title(Const.TEXTS[i])
