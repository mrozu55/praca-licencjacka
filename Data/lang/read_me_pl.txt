﻿
<p>Program przedstawia działanie algorytmu genetycznego, który czerpie inspiracje z genetyki i procesu ewolucji. Pomaga zwizualizować jak poszczególne metody selekcji, różne parametry mutacji czy kombinacji wpływają na działanie algorytmu. Działanie programu polega na wykorzystaniu algorytmów genetycznych do odnalezienia optymalnej ścieżki do celu. Plansze posiadają przeszkody, które algorytm musi nauczyć się omijać. Cel jest przedstawiony jako czerwone koło u dołu planszy. Oprócz 3 podstawowych plansz, użytkownik może stworzyć swoje własne i przetestować na nich działanie algorytmu.
<p>Poszczególne rozwiązania są przedstawione jako pojedyncze kulki (osobniki) w różnych kolorach, które poruszają się po planszy. Kolory osobników są dziedziczone co oznacza, że w późniejszych generacjach pozostaje kilka lub tylko jeden kolor należący do osobnika, który miał najlepszy wynik. Kolory pomagają również w odnajdywaniu rodzin osobników, które pochodzą od tego samego rodzica i poruszają się w bardzo podobny sposób. Program pozwala na wybór 4 popularnych metod selekcji i sprawdzenie jaki mają oni wpływ na działanie algorytmu. Pozwala również na zmianę innych parametrów jak np:. poziom mutacji czy liczba populacji. Użytkownik może także wyłączyć metody krzyżowania lub mutacji. Program zbiera dane na temat każdego osobnika. Informacje te można znaleźć w pliku csv w katalogu ‘Data’. Zebrane informacje można sprawdzić na wykresie dostępnym w menu głównym. Program podczas ponownego uruchomienia czyści zapisane dane. Aplikacja posiada dwa języki do wyboru - polski i angielski.
<p>Podczas pracy programu można zauważyć jednego osobnika, który jest większy od pozostałych. Jest to osobnik z najlepszym wynikiem przystosowania z poprzedniej generacji. Osobnik ten jest kopiowany bez jakichkolwiek modyfikacji jak krzyżowanie lub mutacja. Zapewnia to, że program nie traci aktualnie najlepszego rozwiązania, a w kolejnych generacjach nie uzyskuje gorszego wyniku.

<cat>Menu główne

<c>Plansze

<b>Mapa 1:
Trudniejsza plansza z 3 możliwymi drogami. Najkrótsza droga jest zarazem tą najtrudniejszą do odnalezienia. Zmiana mapy resetuje populacje.

<b>Mapa 2:
Prostsza plansza z 2 przeszkodami i jedną główną drogą do celu. Zmiana mapy resetuje populacje.

<b>Mapa 3:
Plansza z lokalnym maksimum. Problemem algorytmów ewolucyjnych jest to, że często mogą utknąć w lokalnym maksimum. W tej planszy, aby dostać się do celu algorytm musi najpierw się od niego oddalić, aby ominąć przeszkodę.

<c>Metody selekcji

<b>Metoda turniejowa:
Metoda ta wybiera losowo 10 osobników, następnie z wybranej grupy rodzicem zostaje ten z najlepszym wynikiem. Powtarza się tą metodę tyle razy ile wynosi liczba populacji. Gdy metoda jest aktywna, ponowne naduszenie przycisku daję możliwość wprowadzenia parametru, który stanowi liczbę rywalizujących ze sobą osobników w grupie. Większa liczba zmniejsza prawdopodobieństwo wyboru słabszych osobników jednak czasami nie jest to optymalne podejście. W programie jest ustawiona jako domyślna ponieważ daje najlepsze efekty

<b>Metoda ruletki:
Prawdopodobieństwo wylosowania osobnika jest silnie zależne od jego wyniku przystosowania. Osobniki ze słabym wynikiem przystosowania mają niewielką szanse na wylosowanie.

<b>Metoda rankingowa:
Wykorzystuje metodę ruletki jednak przed tym sortuje listę osobników od najsłabszego do najlepszego i zmienia ich wartość przystosowania (od 1 do liczby populacji). Sprawia to, że różnice pomiędzy najsłabszym a najlepszym osobnikiem są mniejsze.

<b>Metoda truncation:
Metoda sortuje listę osobników od najlepszego do najgorszego a następnie wybiera 10% pierwszych osobników czyli tych z najlepszym wynikiem przystosowania. Osobniki te będą rodzicami dla następnej generacji. Metoda ta posiada również możliwość zmiany tego parametru z 10 do maksymalnie 50% populacji

<c>Krzyżowanie i mutacje

<b>Krzyżowanie:
Aktywuje i deaktywuje metodę krzyżowania osobników. Domyślnie metoda jest włączona.

<b>Liczba rodziców:
Pozwala wprowadzić parametr do metody krzyżowania. Parametr jest liczbą rodziców, które tworzą nowego osobnika. Maksymalnie można podać liczbę rodziców równą liczbie populacji lecz nie większą niż 800. Należy mieć na uwadze, że przy dużej liczbie populacji oraz dużej liczbie rodziców czas potrzebny do stworzenia kolejnej generacji może się znacznie wydłużyć.

<b>Mutacja:
Aktywuje i deaktywuje mutacje osobników. Domyślnie metoda jest włączona.

<b>Zmień skale mutacji:
Pozwala na zmianę parametru mutacji. Najniższa możliwa wartość to 0.0001%, a najwyższa 100%. Należy jednak pamiętać, że mutacje już na poziomie kilku procent wprowadzają dużą losowość. Domyślna wartość to 1%.

<c>Pozostałe

<b>Metoda wzrostowa:
Metoda zmienia działanie programu. Ustala maksymalną ilość kroków na 30. Każda kolejna generacja zwiększa maksymalną ilość kroków o 8. Takie podejście sprawia, że algorytm odnajduję najlepsze rozwiązanie na ograniczonej przestrzeni. Dzięki temu możemy szybciej odnaleźć bardziej optymalne rozwiązanie. Nie daje ona dobrych efektów przy mapie nr. 3. Domyślnie metoda ta jest wyłączona. Włączenie jej resetuje populacje do stanu początkowego.

<b>Populacja:
Pozwala zmienić liczbę populacji. Ze względów obliczeniowych parametr powinien być z przedziału 100 – 1000. Większa liczba populacji na słabszych komputerach mogła by znacznie spowolnić prace programu lub całkowicie go zawiesić. Istnieje jednak możliwość podania większej liczby do 5000. Natomiast zbyt mała populacji wydłużyła by proces odnajdywania optymalnej drogi do celu.

<b>Prędkość
Zmienia prędkość poruszania się osobników.

<b>Rozkład populacji
Wyświetla wykres punktowy, który przedstawia jak cała populacja się rozwija. Oś x reprezentuje liczbę kroków, oś y odległość od celu. Ciemniejszy kolor punktów oznacza nowszą generacje. Punkty oznaczone symbolem X to osobniki, które dotarły do celu. Wykres można sprawdzić dopiero po drugiej generacji, gdy zbierze się minimalną ilość danych.

<b>Wykres osobników
Wyświetla dwa wykresy liniowe. Pierwszy przedstawia średni wynik przystosowania osobników dla każdej generacji. Drugi wykres przedstawia liczbę osobników poszczególnych kolorów również dla każdej generacji. Wykres można sprawdzić dopiero po drugiej generacji, gdy zbierze się minimalną ilość danych.

<b>ENG / PL
Zmienia język pomiędzy polskim i angielskim. 

<cat>Pasek boczny

<c>Generacja:
Wyświetla numer generacji .

<c>Kroki:
Wyświetla liczbę kroków najlepszego osobnika z poprzedniej generacji, który dotarł do celu. Jeżeli, żaden osobnik nie osiągnął celu wyświetlany jest myślnik.

<c>Start / Stop:
Rozpoczyna i zatrzymuje pracę programu.

<c>Mutacja:
Wskazuje aktualny parametr mutacji. Gdy zostanie wprowadzona zbyt duża wartość kolor tekstu zmieni się odpowiednio na pomarańczowy lub czerwony

<c>Populacja:
Wskazuje aktualną liczbę populacji. Kolor czcionki zależny jest od parametru.

<c>L. rodziców:
Wskazuje na aktualną liczbę rodziców, z których powstaję nowy osobnik. Kolor czcionki również jest zależny od parametru.


<cat>Tryb edycji

Pozwala użytkownikowi na stworzenia własnej planszy. Przeszkody rysuje się przy pomocy myszki. Można je dowolnie umieszczać, jednak nie mogą kolidować z punktem startu i celu.  W trybie edycji można przesuwać punkt startu i celu, cofać zmiany, anulować oraz wyczyścić plansze. Klikniecie przycisku A anuluje zmiany i wychodzi z trybu edycji, Przycisk Z cofa ostatnie zmiany, a przycisk D czyści plansze z przeszkód. Po stworzeniu planszy i umiejscawianiu punktu startu i celu należy ponownie kliknąć spacje, by zatwierdzić zmiany.

<cat>Wskazówki

Jeżeli chcesz szybko uzyskać wyniki zwiększ prędkość poruszania się osobników ustawiając prędkość na 3.

Gdy algorytm nie może odnaleźć drogi do celu spróbuj zwiększyć wielkość populacje (maksymalnie 5000). Zwiększy to przeszukiwaną przestrzeń możliwych rozwiązań, jednak może spowolnić prace programu. Jeżeli to nie zadziała należy zresetować populacje. Można także zwiększyć poziom mutacji o kilka procent.

Klikając przycisk X można sprawdzić ścieżkę najlepszego osobnika. Funkcja ta działa dopiero, gdy przynajmniej jeden z osobników dotarł do celu.


