
<p>The program presents the performance of a genetic algorithm that draws inspiration from genetics and the evolution process. It helps to visualize how individual selection methods, different parameters of mutations or combinations affect the algorithm. The program is about using genetic algorithms to find the optimal path to the target. Maps contain obstacles that the algorithm must learn to avoid. The target is represented by a red ball at the bottom of the board. Apart from the 3 basic maps, the user can create his own maps and test the algorithm on them.
<p>Individual solutions are represented by single balls (individuals) in different colours that move around the map. The colours of the individuals are inherited, which means that in later generations several or only one colour remains belonging to the individual who had the best result. Colours also help in finding families of individuals who come from the same parent and move in a very similar way. The program allows user to choose 4 popular selection methods and check how they affect the algorithm. It also allows changing other parameters such as: mutation rate or population size. The user also can turn off the crossover or the mutation method. The program collects data about each individual. This data can be found in the csv file inside the 'Data' directory. The collected data can be displayed on the chart located in the main menu. The program cleans saved data while restarting. The application has two languages - Polish and English.
<p>While the program is running you can see one individual that is bigger than the others. It is an individual with the best fitness value from the previous generation. This individual is copied without any modification such as mutation or crossover. This ensures that the program does not lose currently the best solution and does not get worse results in new generations.

<cat>Main menu 

<c>Maps

<b>Map 1:
It is a more difficult map with 3 possible roads. The shortest road is also the most difficult one to find. Changing a map resets the population.

<b>Map 2:
The simpler map with 2 obstacles and one main road to the goal. Changing a map resets the population.

<b>Map 3:
The map with the local maximum. The problem with evolutionary algorithms is that they often can get stuck in the local maximum. On this map, to get to the goal the algorithm must first move away from it to avoid the obstacle.

<c>Selection method

<b>Tournament method:
This method randomly selects 10 individuals, then from the selected group, the parent is the one with the best result. This method is repeated as many times as the population size is. When this method is active, the button can be pressed again to enter the parameter which is the number of competing individuals in the group. A higher number reduces the probability of selecting weaker individuals, however, sometimes this is not an optimal approach. This method is set as default because it gives the best results.

<b>Roulette method:
The probability of an individual being drawn is strongly dependent on the fitness value. Individuals with weak fitness have little chance of being drawn.

<b>Ranking method:
It uses the roulette method, however, before that, it sorts the list of individuals from the weakest to the best and changes their fitness value (from 1 to population size). This reduces the differences between the weakest and the best individual.

<b>Truncation method:
This method sorts the list of individuals from the best to the worst and then selects 10% of the first individuals, i.e. those with the best fitness value. These individuals will be parents for the next generation. The method also has the ability to change this parameter from 10 to maximum 50% of the population.

<c>Crossover & mutation

<b>Crossover:
It enables and disables the crossover method. By default, the method is enabled.

<b>Parents number:
It allows to enter a parameter for the crossover method. The parameter is the number of parents that create a new individual. A maximum number of parents can be equal to the number of the population but not greater than 800. It should be kept in mind that with a large population and a large number of parents, the time needed to create a next generation might be much longer.

<b>Mutation:
It enables and disables the mutation method. By default, the method is enabled.

<b>Mutation rate:
It allows changing the mutation parameter. The lowest possible value is 0.0001% and the highest 100%. It should be remembered, that mutation at rate even of a few percent creates high randomness. The default value is 1%.

<c>Others

<b>Incremental method:
This method changes the operation of the program. It sets the maximum number of steps to 30. Each next generation increases the maximum number of steps by 8. This approach makes the algorithm find the best solution in a limited space. It allows finding a more optimal solution faster. However, It does not give good results on the map no. 3. By default, this method is disabled. Enabling it resets the populations to their initial state.

<b>Population:
Allows changing the population size. For computational reasons, the parameter should be between 100 - 1000. A larger number of population on weaker computers could significantly slow down the program or crash it. However, it is possible to give a larger number up to 5000. Whereas giving too small population would slow down the process of finding the optimal path to the goal.

<b>Speed
It changes movement speed of individuals.

<b>Population plot
Displays a scatter plot that shows how the whole population is evolving. The x-axis represents the number of steps, the y-axis represents the distance from the target. The darker colour of points indicates a newer generation. Points marked with an X are individuals who have reached a goal. The plot can be checked only after the second generation, when the minimum amount of data is collected.

<b>Individuals graph
Displays two line graphs. The first one shows the average fitness value of the individuals for each generation. The second graph shows the number of individuals of each colour also for each generation. The graph can be checked only after the second generation, when the minimum amount of data is collected.

<b>PL / ENG
It changes the language between Polish and English.

<cat>Side bar

<c>Generation:
Display generation number.

<c>Steps:
Display the steps number of the best individual from the previous generation who has reached the goal. If no one has reached the goal, it shows hyphen.

<c>Start / Stop:
It starts and stops the program.

<c>M. Rate:
Indicates the current mutation parameter. If the value is too large, the font colour will change to orange or red respectively.
 
<c>Population:
Indicates the current population size. The font colour also depends on the parameter.
 
<c>Parents nr.:
Indicates the current number of parents that creates new individual. The font colour also depends on the parameter.
 
<cat>Edit mode

It allows to create a new map. Obstacles are drawn using the mouse. They can be placed freely, but they cannot collide with the starting point and target.  In the edit mode user can move the starting point and the goal, undo changes, cancel or clear the board. Clicking A cancels the changes and exits from edit mode, the Z button cancels the last changes and the D button clears the boards from obstacles. Once the board has been created and the starting point and goal have been positioned, click the spacebar to confirm the changes.

<cat>Hints

If you want to get the results quickly, increase the movement speed of the individuals by setting the speed to 3.

When the algorithm cannot find a way to the goal, try to increase the population size (maximum 5000). This will increase the search space of possible solutions, but may slow down the program. If this does not work, reset the population. You can also increase the mutation rate by a few percents.

By clicking the X button, you can check the path of the best individual. This function only works when at least one of the individuals has reached the goal.

