import tkinter as tk
import arcade
import Const


class Window:
    def __init__(self):
        self.master = tk.Tk()
        self.strDialogResult = ''
        self.set_window(320, 150)
        self.master.protocol('WM_DELETE_WINDOW', self.exit)
        self.master.resizable(width=False, height=False)

    def set_window(self, width, height):
        (x, y) = arcade.get_window().get_location()
        x = int(x + Const.SCREEN_WIDTH / 2 - int(width/2))
        y = int(y + Const.SCREEN_HEIGHT / 2 - int(height/2))
        self.master.geometry("{}x{}+{}+{}".format(width, height, abs(x), abs(y)))

    def confirm(self, description):
        self.set_window(350, 180)
        self.strDialogResult = "No"
        self.master.wm_title(Const.TEXTS[50])
        frame = tk.Frame(self.master, relief=tk.RAISED, borderwidth=1)
        frame.pack(fill=tk.BOTH, expand=True)

        tk.Label(frame, text=description, font='12').pack(side=tk.TOP, padx=5, pady=5)
        tk.Label(frame, text=Const.TEXTS[51], font='12').pack(side=tk.TOP, padx=5, pady=5)
        close_button = tk.Button(self.master, text=Const.TEXTS[47], font='12', width=7, command=self.master.quit)
        close_button.pack(side=tk.RIGHT, padx=5, pady=5)
        okButton = tk.Button(self.master, text=Const.TEXTS[48], font='12', width=8, command=lambda: self.DialogResult('Yes'))
        okButton.pack(side=tk.RIGHT, padx=5, pady=5)
        self.master.focus_force()
        self.master.mainloop()
        self.master.destroy()
        return self.strDialogResult

    def instruction(self, file):
        self.set_window(Const.FULL_SCREEN_WIDTH, Const.SCREEN_HEIGHT)
        self.master.wm_title(Const.TEXTS[52])

        frame = tk.Frame(self.master, relief=tk.RAISED, borderwidth=1)
        frame.pack(fill=tk.BOTH, expand=True)

        txt = tk.Text(frame, state='normal', wrap='word')
        txt.pack(side=tk.LEFT, expand=True, fill='both', padx=5, pady=5)
        scrollb = tk.Scrollbar(frame, command=txt.yview)
        scrollb.pack(side=tk.RIGHT, fill='both')
        txt['yscrollcommand'] = scrollb.set

        txt.tag_configure('text_body', font=('Times', 13), lmargin1=35, lmargin2=20)
        txt.tag_configure('p', font=('Times', 13), lmargin1=25, lmargin2=1)
        txt.tag_configure('button', font=('Times', 15), lmargin1=10, lmargin2=0)
        txt.tag_configure('section', font=('Times bold', 15), lmargin1=1, lmargin2=1)
        txt.tag_configure('category', font=('Times bold', 16), justify='center', lmargin1=0, lmargin2=0)

        try:
            linia = file.readline()
            while linia:
                if linia.find("<b>") != -1:
                    txt.insert(tk.END, linia[3:len(linia)], 'button')
                elif linia.find("<cat>") != -1:
                    txt.insert(tk.END, linia[5:len(linia)], 'category')
                elif linia.find("<c>") != -1:
                    txt.insert(tk.END, linia[3:len(linia)], 'section')
                elif linia.find("<p>") != -1:
                    txt.insert(tk.END, linia[3:len(linia)], 'p')
                else:
                    txt.insert(tk.END, linia, 'text_body')
                linia = file.readline()
        except IOError:
            Window().prompt(Const.TEXTS[29], Const.TEXTS[53])
        finally:
            file.close()

        txt.configure(state='disabled')
        close_button = tk.Button(self.master, text=Const.TEXTS[49], font='12', width=6, command=self.master.quit)
        close_button.pack(side=tk.RIGHT, padx=5, pady=5)
        self.master.focus_force()
        self.master.mainloop()
        self.master.destroy()

    def prompt(self, title, description):
        self.master.wm_title(title)
        frame = tk.Frame(self.master, relief=tk.RAISED, borderwidth=1)
        frame.pack(fill=tk.BOTH, expand=True)
        tk.Label(frame, text=description, font='12').pack(side=tk.TOP, padx=5, pady=5)

        close_button = tk.Button(self.master, text='OK', font='12', width=6, command=self.master.quit)
        close_button.pack(side=tk.RIGHT, padx=5, pady=5)
        self.master.focus_force()
        self.master.mainloop()
        self.master.destroy()

    def input(self, title, description=""):
        self.master.wm_title(title)

        frame = tk.Frame(self.master, relief=tk.RAISED, borderwidth=1)
        frame.pack(fill=tk.BOTH, expand=True)

        tk.Label(frame, text=description).pack(side=tk.TOP, padx=5, pady=5)
        tk.Label(frame, text=Const.TEXTS[54], font='12').pack(side=tk.LEFT, padx=20, pady=5)

        e1 = tk.Entry(frame, font='serif 20')
        e1.pack(side=tk.RIGHT, padx=5, pady=5)
        e1.focus_force()
        closeButton = tk.Button(self.master, text=Const.TEXTS[49], font='12', width=6, command=self.master.quit)
        closeButton.pack(side=tk.RIGHT, padx=5, pady=5)
        okButton = tk.Button(self.master, text="OK", font='12', width=6, command=lambda: self.DialogResult(e1.get()))
        okButton.pack(side=tk.RIGHT, padx=5, pady=5)

        self.master.mainloop()
        self.master.destroy()
        return None if self.strDialogResult == '' else self.strDialogResult

    def DialogResult(self, result):
        self.strDialogResult = result
        self.master.quit()

    def exit(self):
        self.master.quit()

