import arcade
import math
import Const


class Dot(arcade.Sprite):
    """Dot"""

    def __init__(self, image, scale):
        """setting up dot"""
        super().__init__(image, scale)
        self.speed = 0
        self.brain = None
        self.dead = False
        self.winner = False  # Czy dotarł do celu
        self.is_best = False  # najlepszy z poprzedniej generacji
        self.fitness = 0.0  # Wartość przystosowania
        self.image_nr = 0  # kolor osobnika

    def add_brain(self):
        from Brain import Brain
        self.brain = Brain(Const.STEPS)

    def create_chromosome(self, feature):
        a = 1 if feature == 0 else feature * 3
        self.brain.update(a, 0.50)

    def is_the_best(self):
        self.is_best = True
        self.scale = Const.SPRITE_SCALING + 0.5

    def update(self):
        self.angle += self.change_angle
        self.move()
        for a in Const.OBSTACLES:
            if a.right > self.center_x > a.left and a.top > self.center_y > a.bottom:
                self.dead = True
                return
        if self.center_x < 2 or self.center_y < 2 \
                or self.center_x > Const.SCREEN_WIDTH - 2 or self.center_y > Const.SCREEN_HEIGHT - 2:
            self.dead = True
        # sprawdzenie czy osobnik dotarł do celu
        elif self.distance() < 6:
            self.winner = True

    def move(self):
        if self.brain.maxSteps > self.brain.step:
            angle_rad = math.radians(self.angle)
            self.center_x += self.speed * math.sin(angle_rad)
            self.center_y += -self.speed * math.cos(angle_rad)
            self.change_angle = self.brain.direction[self.brain.step]
            self.brain.step += 1
        else:
            self.dead = True

    def distance(self) -> float:
        """Obliczanie dystansu do celu"""
        a = abs(self.center_x - Const.GOAL_X)
        b = abs(self.center_y - Const.GOAL_Y)
        return math.sqrt(a * a + b * b)

    def calculate_fitness(self):
        if self.winner:
            fitness = 0.05 + 10000.0 / (self.brain.step * self.brain.step)
        else:
            odl = self.distance()
            fitness = 1.0 / (self.brain.step / 2 + odl * odl)
        if self.fitness < fitness:
            self.fitness = fitness

    def copy(self):
        child = Dot(Const.IMAGES[self.image_nr], Const.SPRITE_SCALING)
        child.image_nr = self.image_nr
        child.brain = self.brain.clone_code(Const.MIN_STEPS)
        child.brain.maxStep = Const.MIN_STEPS
        return child

    def __del__(self):
        del self.speed
        del self.brain
        del self.dead
        del self.winner
        del self.is_best
        del self.fitness
        del self.image_nr

# if feature < 3:
#     self.brain.update(1 + a * 2, 0.55)
# elif feature < 6:
#     self.brain.update(8 + a * 5, 0.6)
# else:
#     self.brain.update(24 + a * 4, 0.25)