import arcade


class Obstacle:
    def __init__(self, left, right, top, bottom):
        self.left = left
        self.right = right
        self.top = top
        self.bottom = bottom
        self.obstacle = None
        self.setup()

    def setup(self):
        center_x = (self.left + self.right)/2
        center_y = (self.top + self.bottom)/2
        width = self.right-self.left
        height = self.top - self.bottom
        self.obstacle = arcade.create_rectangle(center_x, center_y, width, height, arcade.color.BLUSH)

    def show(self):
        self.obstacle.draw()

    def __del__(self):
        del self.left
        del self.right
        del self.top
        del self.bottom
        del self.obstacle

