import Obstacle
import Const


class Map:
    def __init__(self):
        self.obstacles = []
        self.map_nr = 1
        self.setup()

    def setup(self):
        if self.map_nr == 1:
            self.map_one()
        elif self.map_nr == 2:
            self.map_two()
        else:
            self.map_three()
        Const.OBSTACLES = self.obstacles
        Const.MIN_STEPS = Const.STEPS

    def set_user_map(self, obstackles):
        del self.obstacles
        self.obstacles = obstackles
        self.map_nr = 0
        Const.STEPS = 800
        Const.OBSTACLES = self.obstacles
        Const.MIN_STEPS = Const.STEPS

    def map_one(self):
        self.addObstacle(170, Const.SCREEN_WIDTH, Const.SCREEN_HEIGHT - 150, Const.SCREEN_HEIGHT - 170)
        self.addObstacle(0, 270, 270, 250)
        self.addObstacle(290, 420, 270, 250)
        self.addObstacle(260, 270, 250, 170)
        self.addObstacle(280, 300, 200, 50)
        Const.STEPS = 600

    def map_two(self):
        self.addObstacle(250, Const.SCREEN_WIDTH, 450, 430)
        self.addObstacle(0, Const.SCREEN_WIDTH - 250, 200, 180)
        Const.STEPS = 600

    def map_three(self):
        self.addObstacle(0, 435, 450, 430)
        self.addObstacle(415, 435, 430, 150)
        self.addObstacle(230, 250, 280, 0)
        Const.STEPS = 800

    def addObstacle(self, l, r, t, b):
        self.obstacles.append(Obstacle.Obstacle(l, r, t, b))

    def show(self):
        for a in self.obstacles:
            a.show()

    def change_map(self, nr):
        """Change between 3 default maps"""
        if self.map_nr == 0:
            self.reset_settings() # if it was user map reset start and finish point
        del self.obstacles
        self.obstacles = []
        self.map_nr = nr
        self.setup()

    def reset_settings(self):
        Const.START_X = Const.SCREEN_WIDTH / 2
        Const.START_Y = Const.SCREEN_HEIGHT - 10
        Const.GOAL_Y = 15
        Const.GOAL_X = 200

    def __del__(self):
        del self.obstacles
