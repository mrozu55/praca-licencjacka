from Graph import Graph
from UserMap import UserMap
from Menu import MainMenu
import arcade
import Population
import Const
import Map
import Button
import gc


def check_collision(x, y, obj_x, obj_y):
    if obj_x + 10 > x > obj_x - 10 and obj_y + 10 > y > obj_y - 10:
        return True
    else:
        return False


class GeneticAl(arcade.Window):
    """Main class"""

    def __init__(self, width, height):
        super().__init__(width, height, title="Genetic Algorithm")
        self.goal = None
        self.graph = Graph("Data/Population.csv")
        self.pop = Population.Population()
        self.generation = 1
        self.steps = "-"
        self.wall = None
        self.map = Map.Map()
        self.only_the_best = False
        self.pause = True
        self.edit_mode = False
        self.user_map = None
        self.menu = MainMenu(self)
        self.mut_color = arcade.color.WHITE
        self.pop_color = arcade.color.WHITE
        self.cross_color = arcade.color.WHITE
        self.speed = 1  # 1 - 2 - 3
        self.tick = 0
        arcade.set_background_color(arcade.color.BLACK)

    def setup_goal(self):
        self.goal.center_x = Const.GOAL_X
        self.goal.center_y = Const.GOAL_Y

    def setup(self):
        """ Set up the game and initialize the variables. """
        self.graph.make_csv()  # utworzenie lub wyczyszczenie plku csv
        self.goal = arcade.Sprite("Images/GoalDot.png", 1)
        self.setup_goal()
        self.wall = arcade.create_rectangle(Const.SCREEN_WIDTH + 5, Const.SCREEN_HEIGHT / 2, 12,
                                            Const.SCREEN_HEIGHT, arcade.color.BLUSH)
        self.pop.setup()
        self.menu.setup()

    def on_draw(self):
        """drawing code"""
        arcade.start_render()
        self.wall.draw()
        if self.edit_mode:
            self.user_map.show()
            if self.user_map.drawing:
                self.user_map.show_drawing()
            return
        arcade.draw_text("S.M", Const.SCREEN_WIDTH + 6, 16, arcade.color.BLACK, 11, rotation=90, align="center", anchor_x="center")
        arcade.draw_text(str(self.generation), 675, 500, arcade.color.WHITE, 30, align="center", anchor_x="center")
        arcade.draw_text(Const.TEXTS[18], 675, 460, arcade.color.WHITE, 14, align="center", anchor_x="center")
        arcade.draw_text(str(self.steps), 675, 360, arcade.color.WHITE, 30, align="center", anchor_x="center")
        arcade.draw_text(Const.TEXTS[19], 675, 320, arcade.color.WHITE, 14, align="center", anchor_x="center")
        arcade.draw_text(Const.TEXTS[20] + ((str(Const.MUTATION_RATE) + "%") if Const.MUTATION else Const.TEXTS[28]),
                         620, 150, self.mut_color, 14)
        arcade.draw_text(Const.TEXTS[21] + str(Const.POPULATION), 620, 121, self.pop_color, 14)
        arcade.draw_text(Const.TEXTS[22] + (str(Const.PARENTS_NR) if Const.CROSSOVER else Const.TEXTS[28]), 620,
                         92, self.cross_color, 14)
        self.menu.draw_start()

        if self.pause:
            """Draw main menu"""
            self.menu.draw()
            return
        if self.pop.found_goal:
            arcade.draw_text(Const.TEXTS[75] if self.only_the_best else Const.TEXTS[74], 620, 12, arcade.color.WHITE, 12)
        self.map.show()
        self.goal.draw()  # rysowanie celu
        if self.only_the_best is False:
            self.pop.dots_list.draw()  # rysowanie populacji
        else:
            self.pop.draw_the_best()

    def update(self, delta_time: float):
        """Stop the program"""
        if self.pause or self.edit_mode:
            return
        """Sprawdź czy populacja umarła"""
        if self.pop.population_dead() is True:
            self.pop.calculate_fitness()
            self.pop.find_the_best()
            self.graph.update_csv(self.pop.dots_list, self.generation)
            self.pop.natural_selection()
            self.generation += 1
            self.tick = 0
            if self.pop.min_step != Const.STEPS:
                self.steps = self.pop.min_step
            gc.collect()
        else:
            self.pop.update()
            if self.tick % 8 == 0:
                self.pop.check_fitness()
            self.tick += 1

    def on_key_press(self, key, modifiers):
        if key == arcade.key.SPACE:
            if self.edit_mode:
                """Disable edit mode"""
                self.edit_mode = False
                self.user_map.confirm()
                self.map.set_user_map(self.user_map.get_obstacles())
                self.menu.restart()
                self.setup_goal()
                if self.pause:
                    self.menu.pause_program()
            else:
                """Enable edit mode"""
                if self.user_map is None:
                    self.user_map = UserMap()
                self.edit_mode = True
                self.user_map.set_points()
        elif key == arcade.key.X:
            if self.pop.found_goal:
                self.only_the_best = False if self.only_the_best else True
        if self.edit_mode:
            if key == arcade.key.D:
                self.user_map.clear()
            if key == arcade.key.Z:
                self.user_map.undo()
            if key == arcade.key.A:
                self.edit_mode = False
                self.user_map.cancel()

    def on_mouse_motion(self, x, y, dx, dy):
        if self.user_map.drawing:
            self.user_map.set_drawing(x, y)
        elif self.user_map.move != 0:
            if self.user_map.move == 1:
                self.user_map.move_start_point(x, y)
            elif self.user_map.move == 2:
                self.user_map.move_finish_point(x, y)

    def on_mouse_press(self, x, y, button, key_modifiers):
        if self.edit_mode is True:
            if check_collision(x, y, self.user_map.start_x, self.user_map.start_y):
                self.user_map.move = 1
            elif check_collision(x, y, self.user_map.goal_x, self.user_map.goal_y):
                self.user_map.move = 2
            else:
                self.user_map.drawing = True
                self.user_map.set_initial_position(x, y)
        elif self.menu.lock is not True:
            if self.pause:
                Button.check_mouse_press_for_buttons(x, y, self.menu.button_list)
            else:
                Button.check_mouse_press_for_buttons(x, y, self.menu.button_list[0:1])

    def on_mouse_release(self, x, y, button, key_modifiers):
        """Gdy puści sie przycisk myszy"""
        if self.edit_mode is True:
            if self.user_map.move != 0:
                self.user_map.move = 0
            else:
                self.user_map.drawing = False
                self.user_map.set_obstacle()
        if self.menu.lock is not True:
            Button.check_mouse_release_for_buttons(x, y, self.menu.button_list)


def main():
    game = GeneticAl(Const.FULL_SCREEN_WIDTH, Const.SCREEN_HEIGHT)
    game.setup()
    arcade.run()



if __name__ == "__main__":
    main()
