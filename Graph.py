from matplotlib.ticker import MaxNLocator
from os import path
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import Const
import csv
import WN


class Graph:
    def __init__(self, path):
        self.colors = ['Aqua', 'Blue', 'Mint', 'Orange', 'Pink', 'Red', 'Violet', 'White', 'Yellow']
        self.file_path = path
        self.id = 0

    def restart(self):
        self.id = 0
        self.make_csv()

    def make_csv(self):
        try:
            with open(self.file_path, 'w', newline='') as file:
                writer = csv.writer(file)
                writer.writerow(['#', 'Generation', 'Steps', 'Distance', 'Result', 'Winner', 'Color'])
        except PermissionError:
            WN.Window().prompt(Const.TEXTS[29], Const.TEXTS[57])
            exit(0)

    def plot_method(self):
        try:
            sns.set_style("whitegrid")
            df = pd.read_csv(self.file_path, encoding="ISO-8859-1", index_col=0)
            g = sns.relplot(x='Steps', y='Distance', data=df, size="Result", sizes=(15, 300), style="Winner",
                            hue="Generation")
            g.fig.set_size_inches(10, 7)
            g.fig.canvas.set_window_title(Const.TEXTS[55])
            g.set(xlabel=Const.TEXTS[63], ylabel=Const.TEXTS[64], title=Const.TEXTS[65])
            plt.show()
        except ValueError:
            WN.Window().prompt(Const.TEXTS[29], Const.TEXTS[56])
        except PermissionError:
            WN.Window().prompt(Const.TEXTS[29], Const.TEXTS[57])
        except FileNotFoundError:
            WN.Window().prompt(Const.TEXTS[29], Const.TEXTS[58])
        finally:
            plt.close()

    def plot2_method(self):
        sns.set_style("darkgrid")
        # Aqua  # Blue # Mint # Orange # Pink # Red # Violet # White # Yellow
        type_colors = ['#3ad198', '#5199b9', '#46d13a', '#d1783a', '#cf3b6b', '#d13a3a', '#c045c6', '#ffffff', '#d1ca3a']
        try:
            df = pd.read_csv(self.file_path, encoding="ISO-8859-1", index_col=0)
            fig, axs = plt.subplots(ncols=2, figsize=(11, 5))
            fig.canvas.set_window_title(Const.TEXTS[55])
            graf = sns.lineplot(x="Generation", y="Result",palette=type_colors, hue_order=self.colors, hue="Color", data=df, ci=None, ax=axs[0])
            g = sns.lineplot(x="Generation", y="Result", data=df, color="black", ci=None, ax=axs[0])
            g.lines[19].set_linestyle("-.")
            lines = [g.lines[19]]
            lines += graf.lines
            graf.lines = lines
            g.set(xlabel=Const.TEXTS[66], ylabel=Const.TEXTS[61], title=Const.TEXTS[62])
            g.xaxis.set_major_locator(MaxNLocator(integer=True))
            g.legend(labels=['Mediana','Aqua','Blue','Mint','Orange','Pink','Red','Violet','White','Yellow'])
            counts = df["Color"].groupby(df["Generation"]).value_counts().rename("counts").reset_index()
            g2 = sns.lineplot(x="Generation", y="counts", palette=type_colors, hue_order=self.colors, hue="Color", data=counts, ax=axs[1])
            g2.xaxis.set_major_locator(MaxNLocator(integer=True))
            g2.set(xlabel=Const.TEXTS[66], ylabel=Const.TEXTS[59], title=Const.TEXTS[60])
            plt.show()
        except ValueError:
            WN.Window().prompt(Const.TEXTS[29], Const.TEXTS[56])
        except PermissionError:
            WN.Window().prompt(Const.TEXTS[29], Const.TEXTS[57])
        except FileNotFoundError:
            WN.Window().prompt(Const.TEXTS[29], Const.TEXTS[58])
        finally:
            plt.close()

    def update_csv(self, dots_list, generation):
        while True:
            if path.exists(self.file_path) is False:
                self.make_csv()
            try:
                with open(self.file_path, 'a', newline='') as file:
                    writer = csv.writer(file)
                    for a in dots_list:
                        writer.writerow([self.id, generation, a.brain.step, a.distance(), a.fitness, a.winner, self.colors[a.image_nr]])
                        self.id += 1
                    break
            except PermissionError:
                WN.Window().prompt(Const.TEXTS[29], Const.TEXTS[57])

