import Dot
import Const
import arcade
import random


class Population:
    def __init__(self):
        self.dots_list = arcade.SpriteList()
        self.the_best = 0  # indeks najlepszego osobnika
        self.min_step = Const.STEPS
        self.incremental = False  # metoda wzrostowa
        self.found_goal = False

    def setup(self):
        self.set_dots()

    def set_dots(self):
        """Ustawienie wszystkich osobników"""
        for j in range(Const.POPULATION):
            rand_image = int(random.random() * 9)
            dot = Dot.Dot(Const.IMAGES[rand_image], Const.SPRITE_SCALING)
            dot.center_x = Const.START_X
            dot.center_y = Const.START_Y
            dot.image_nr = rand_image
            dot.add_brain()
            dot.create_chromosome(rand_image)  # nadanie losowych kierunków poruszania sie chromosom jest zalezny od koloru
            dot.speed = Const.SPEED
            self.dots_list.append(dot)  # dodanie osobnika do listy

    def restart_population(self):
        self.dots_list = arcade.SpriteList()
        self.the_best = 0
        self.found_goal = False
        self.min_step = Const.STEPS
        Const.MIN_STEPS = Const.STEPS
        self.set_dots()

    def second_initial(self):
        """Uruchamiana przy tworzeniu kolejnych generacji"""
        for j in range(len(self.dots_list)):
            self.dots_list[j].center_x = Const.START_X
            self.dots_list[j].center_y = Const.START_Y
            self.dots_list[j].speed = Const.SPEED

    def update(self):
        """Update osobników"""
        for i in range(len(self.dots_list)):
            if self.dots_list[i].dead is False and self.dots_list[i].winner is False:
                if self.incremental is False:
                    self.dots_list[i].update()
                else:
                    if self.min_step > self.dots_list[i].brain.step:
                        self.dots_list[i].update()
                    else:
                        self.dots_list[i].dead = True

    def population_dead(self):
        """Sprawdza czy cala populacja jeszcze żyje - wyczerpała punkty ruchu,
           dotarla do celu lub uderzyła w przeszkode"""
        for a in self.dots_list:
            if a.dead is False and a.winner is False:
                return False
        return True

    def incremental_method(self):
        """Metoda wzrostowa"""
        if self.incremental:
            self.incremental = False
            self.min_step = Const.STEPS
        else:
            self.incremental = True
            self.min_step = 30

    def natural_selection(self):
        parentlist = []  # lista osobników wybranych jako rodzice
        self.find_the_best()
        # Zapamietanie najlepszego odobnika z poprzedniej generacji
        osobnik = self.dots_list[self.the_best].copy()
        osobnik.is_the_best()

        # wybor rodzicow - umieszczenie ich w tablicy parent
        if Const.PARENT_SELECTION == 1:
            """Metoda turnejowa"""
            for i in range(Const.POPULATION - 1):
                parent = self.tournament_selection()
                parentlist.append(parent.copy())
        elif 2 <= Const.PARENT_SELECTION <= 3:
            """Metoda ruletki i metoda rankingowa"""
            fitness_sum = self.calculate_fitness_sum()
            for i in range(Const.POPULATION - 1):
                parent = self.roulette_wheel_selection(fitness_sum)
                parentlist.append(parent.copy())
        elif Const.PARENT_SELECTION == 4:
            """Trancate selection"""
            dots_list = sorted(self.dots_list, key=lambda dot: dot.fitness)
            dots_list.reverse()
            range_ = int(len(dots_list) * Const.TRUNK_PARAM)
            parents = dots_list[0: range_]
            for i in range(Const.POPULATION):
                parentlist.append(parents[i % len(parents)].copy())

        self.dots_list = arcade.SpriteList()  # czyszczenie listy ze starych osobnikow
        self.dots_list.append(osobnik)  # Dodanie najlepszego odobnika z poprzedniej generacji
        # krzyzowanie genow i tworzenie potomkow
        if Const.CROSSOVER:
            for a in range(Const.POPULATION - 1):
                self.dots_list.append(self.crossover(Const.PARENTS_NR, parentlist))
        else:
            for a in parentlist:
                self.dots_list.append(a)

        if Const.MUTATION:
            self.set_mutation()

        self.second_initial()
        if self.incremental and self.found_goal is False:
            self.min_step += 8

    def calculate_fitness_sum(self):
        fitness_sum = 0.0
        for a in self.dots_list:
            fitness_sum += a.fitness
        return fitness_sum

    def calculate_fitness(self):
        for a in self.dots_list:
            a.calculate_fitness()
        if Const.PARENT_SELECTION == 3:
            self.rang_selection()

    def find_the_best(self):
        """Odnajdywanie najlepszego osobnika w populacji"""
        maks = 0.0
        indeks = 0  # Indeks najlepszego osobnika
        for i in range(len(self.dots_list)):
            if self.dots_list[i].fitness > maks:
                maks = self.dots_list[i].fitness
                indeks = i
        self.the_best = indeks
        """Ustawianie limitu kroków podlog najlepszego osobnika ktory osiągnał cel"""

        if self.dots_list[self.the_best].winner:
            self.min_step = self.dots_list[self.the_best].brain.step
            Const.MIN_STEPS = self.min_step
            self.found_goal = True

    def roulette_wheel_selection(self, fitness_sum):
        """Selekcja najlepszych metodą ruletki"""
        rand = random.random() * fitness_sum
        sum_ = 0
        for a in self.dots_list:
            sum_ += a.fitness
            if sum_ > rand:
                return a
        return None

    def rang_selection(self):
        dots_list = sorted(self.dots_list, key=lambda dot: dot.fitness)
        i = 1
        for a in dots_list:
            a.fitness = i
            i += 1
        self.dots_list = dots_list

    def tournament_selection(self):
        """metoda turnejowa"""
        indexes = []
        for a in range(Const.TOURNAMENT_PARAM):
            indexes.append(int(random.random() * len(self.dots_list)))

        tempdot = self.dots_list[indexes[0]]
        for a in range(len(indexes) - 1):
            if tempdot.fitness < self.dots_list[indexes[a + 1]].fitness:
                tempdot = self.dots_list[indexes[a + 1]]
        return tempdot

    def crossover(self, q, parents):
        indexes = []
        p = 0
        for a in range(q):
            indexes.append(int(random.random() * len(parents)))  # losowy wybór q rodziców
        child = parents[indexes[0]].copy()
        for i in range(q - 1):
            p = random.randint(p, child.brain.maxSteps - 1)  # punkt przecięcia
            """pobieranie części genów od kolejnego rodzica"""
            for j in range(len(child.brain.direction) - p):
                child.brain.direction[j + p] = parents[indexes[i + 1]].brain.direction[j + p]
        return child

    def set_mutation(self):
        for i in range(len(self.dots_list) - 1):
            self.dots_list[i + 1].brain.mutate(Const.MUTATION_RATE)

    def draw_the_best(self):
        self.dots_list[0].draw()

    def check_fitness(self):
        for a in self.dots_list:
            if a.dead is False:
                a.calculate_fitness()
