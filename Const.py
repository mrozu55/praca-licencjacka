FULL_SCREEN_WIDTH = 750
SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600
SPRITE_SCALING = 0.5
POPULATION = 800
STEPS = 600
MIN_STEPS = STEPS
START_X = SCREEN_WIDTH / 2
START_Y = SCREEN_HEIGHT - 10
GOAL_Y = 15
GOAL_X = 200
SPEED = 3
OBSTACLES = []
PARENTS_NR = 2  # z ilu rodziców powstaje jeden potomek
CROSSOVER = True
MUTATION = True
MUTATION_RATE = 1
TOURNAMENT_PARAM = 10
TRUNK_PARAM = 0.1
PARENT_SELECTION = 1 # okresla metode wyboru rodzica 1 - metoda turniejowa 2 - metoda ruletki 3 - rankingowa 4 - trunk
LANG = 0
IMAGES = ["Images/dotAqua.png", "Images/dotBlue.png", "Images/dotMint.png", "Images/dotOrange.png", "Images/dotRed.png",
          "Images/dotPink.png", "Images/dotViolet.png","Images/dotWhite.png", "Images/dotYellow.png"]
TEXTS = ['Start', 'Mapa 1', 'Mapa 2', 'Mapa 3', 'Metoda \nturniejowa', 'Metoda \nruletki', 'Metoda \nrankingowa', 'Metoda \nTruncation',' Krzyżowanie \n' , ' Liczba\nrodziców', ' Mutacja \n', ' Zmień skale\n mutacji', ' Metoda \n wzrostowa', ' Populacja','Prędkość\n',
         'Instrukcja', 'Wykres\nosobników', ' Rozkład\npopulacji', '.:Generacja:.', '.:Kroki:.', 'Mutacja: ', 'Populacja: ', 'L. rodziców: ',  'Plansze', 'Metody selekcji', 'Krzyżowanie i mutacje', 'Pozostałe',
         'TAK', 'NIE', 'Błąd', 'Ostrzeżenie', 'Zmiana parametru','Proszę podać parametr dla metody truncation\nAktualnie parametr wynosi: {}%\n[10%, 50%]', 'Proszę podać parametr dla metody turniejowej\nAktualnie parametr wynosi: {}',
         'Podana wartość nie może być\n mniejsza niż ', 'Podano zbyt dużą wartość.\nZostanie ustawiona wartość maksymalna\n', 'Podano nie właściwe dane', 'Proszę podać liczbę rodziców jednego potomka\nLiczba rodziców nie może przekraczać: {}','Proszę podać wartość mutacji \nWartość powinna być z przedziału\n [0, 100]',
         'Podana wartość nie może byc ujemna!', 'Podano zbyt niską wartość mutacji.\nZostanie ustawiona najniższa\nmożliwa wartość','Mutacja na poziome wiekszym niż 5% \n wprowadza zbyt dużą losowość', 'Proszę podać wielkość populacji.\nAktualna populacja: {}',
         'Podana wartość możę spowolnić prace\nprogramu lub w przypadku słabszych\nkomputerów całkowiecie go zawiesić.', 'read_me_pl.txt', 'Nie można odnaleźć pliku', 'Niewystarczająca ilość danych\n\nProszę wcisnąć start, aby zebrać dane', 'Anuluj', 'Potwierdź', 'Wyjdź', 'Zatwierdź zmiany', 'Czy potwierdzasz zmiany?', 'Intrukcja obsługi', 'Wystąpił problem z plikiem', 'Wartość', 'Wykres', 'Brak danych lub dane są niepoprawne', 'Błąd dostępu!\n\nProsze zamknąć plik Population.csv\n i spróbować jeszcze raz',
         'Nie odnaleziono pliku Population.csv', 'Liczba osobników', 'Liczba osobników poszczególnego koloru', 'Wartość przystosowania', 'Średnia wartość przystosowania', 'Liczba kroków', 'Odległośc od celu', 'Rozkład populacji', 'Generacja', 'Tryb\nEdycji', 'D - Wyczyść', 'Z - Cofnij', 'A - Anuluj', 'Spacja - OK', '[SPACJA] - tryb edycji', 'Cel', 'X - Pokaż najlepszy', 'X - Pokaż wszystkie']